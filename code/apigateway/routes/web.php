<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login', 'LoginController@index');
$router->post('/register', 'UserController@register');
$router->get('/auth/{provider}', 'AuthController@redirectToProvider');
$router->get('/auth/{provider}/callback', 'AuthController@handleProviderCallback');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('/fetch', 'UserController@getUser');
    $router->post('/guzlle', 'UserController@guzzleGraphql');
    $router->post('/test1', 'UserController@guzzleTest');
    $router->get("/test", function(\Illuminate\Http\Request $request) {
        return $request->bearerToken();
    });

    $router->get("/testrole", 'UserController@getUser');

    });




//$router->get("/ngrok", 'UserController@ngrok');
//
//$router->get("/heartbeat", function(){
//    return 1;
//});
//
//$router->post("/heartbeat", function(){
//    return "post";
//});


