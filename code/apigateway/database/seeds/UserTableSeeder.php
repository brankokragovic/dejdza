<?php

use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 20; $i++) {
            DB::table('users')->insert([

                'email' => str_random(10) . '@gmail.com',
                'password' => str_random(10),
                'api_token' => str_random(40)

            ]);
        }
    }
}
