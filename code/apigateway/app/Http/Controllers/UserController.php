<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{


    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $hasher = app()->make('hash');
        $email = $request->input('email');
        $password = $hasher->make($request->input('password'));
        $user = User::create([
            'email' => $email,
            'password' => $password,
        ]);


        $res['success'] = true;
        $res['message'] = 'Success register!';
        $res['data'] = $user;
        return response($res);
    }

    public function getUser(Request $request)
    {
        $user = Auth::user();
        $user->givePermissionTo('article-create');
        $role = $user->getRoleNames();
        $permissions = $user->permissions;
        $data = array([ "user" => $user , "role" => $role, "permissions" => $permissions]);
        return $data;
    }

    public function guzzleGraphql(Request $request){

        $client = new \GuzzleHttp\Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $params = [ 'query' => $request->all()['query'], 'user' => Auth::user() ];
                                        
        $res = $client->request('POST', 'http://service:1215/fetch',

        ['body' => json_encode($params)]


        );

       return $res->getBody();

    }

    public function guzzleTest(Request $request)
    {

         $client = new \GuzzleHttp\Client([
                    'headers' => [ 'Content-Type' => 'application/json' ]
                ]);

                $params = [ "query" => $request->all()["query"], "user" => Auth::user() ];

                $res = $client->request('GET', 'http://service:1215/test',

                ['body' => json_encode($params)]


                );

               return $res->getBody();


    }

    public function ngrok()
    {

       $client = new \GuzzleHttp\Client([
               'headers' => [ 'Content-Type' => 'x-www-form-urlencoded' ]
           ]);


         /*  if(!file_exists('./storage/logs/serviceid'))
           {
               $res = $client->request('POST', 'https:/core.revolution.connecting.rs/register',
                   [
                       'form_params' => json_decode(file_get_contents('./service.json'))

                   ]);

               file_put_contents('./storage/logs/serviceid',$res->getBody());
               return $res->getBody();
           }
           else
               {
                   $res = $client->request('POST', 'https:/core.revolution.connecting.rs/heartbeat',
                       [
                           'form_params' => json_decode(file_get_contents('./storage/logs/serviceid'))

                       ]);
                   return $res->getBody();

               }

            */
            return 1;

    }
}