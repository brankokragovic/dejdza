<?php

[
    // Replace this handler if you want to customize your websocket handler
    'handler' => SwooleTW\Http\Websocket\SocketIO\WebsocketHandler::class,
    // Replace it if you want to customize your websocket payload
    'parser' => SwooleTW\Http\Websocket\SocketIO\SocketIOParser::class,

    // You can register your websocket event mapping in this route file
    'route_file' => base_path('routes/websocket.php'),

    // Default middleware for on connect request
    'middleware' => [
        SwooleTW\Http\Websocket\Middleware\DecryptCookies::class,
        SwooleTW\Http\Websocket\Middleware\StartSession::class,
        SwooleTW\Http\Websocket\Middleware\Authenticate::class,
    ],

    // Default room driver, it's `table`(swoole table) by default
    'default' => 'table',

    // Don't forget to add the driver mapping here if you want to use your own driver
    'drivers' => [
        'table' => SwooleTW\Http\Websocket\Rooms\TableRoom::class,
        'redis' => SwooleTW\Http\Websocket\Rooms\RedisRoom::class,
    ],

    // Room driver's settings
    'settings' => [
        // Memory of swoole table is allocated in the very begining of the process.
        // You can't modify it after starting server.
        // So you should set them to proper values
        'table' => [
            'room_rows' => 4096,
            'room_size' => 2048,
            'client_rows' => 8192,
            'client_size' => 2048
        ],
        'redis' => [
            'server' => [
                'host' => env('REDIS_HOST', '127.0.0.1'),
                'password' => env('REDIS_PASSWORD', null),
                'port' => env('REDIS_PORT', 6379),
                'database' => 0,
                'persistent' => true,
            ],
            'options' => [
                //
            ],
            'prefix' => 'swoole:',
        ]
    ],
];