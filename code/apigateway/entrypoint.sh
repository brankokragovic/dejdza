#!/bin/bash

rm ./storage/logs/swoole_http.pid

curl -XPOST -H "Content-type: application/json" -d '{
  "name" : "api_gateway",
  "protocol": "https",
  "gateway" : "core.revolution.connecting.rs",
  "host"  :  "a3cfc873.ngrok.io",
  "heartbeat": "/heartbeat"
  "routes": {
  "register": "/register",
  "users.get": "/users"
  }
  }
}' 'https://core.revolution.connecting.rs/register'

php artisan swoole:http start